from django.shortcuts import render, redirect
from accounts.forms import accountLoginForm, accountSignupForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse


def create_AccountLogin(request):
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = accountLoginForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            user = authenticate(
                username=form.data["username"], password=form.data["password"]
            )
            if user is not None:
                login(request, user)
                return redirect("/")
            else:
                context = {"form": form}
                return render(request, "accounts/login.html", context)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = accountLoginForm()
        context = {"form": form}

    return render(request, "accounts/login.html", context)


def create_AccountLogout(request):
    if request.user.is_authenticated:
        logout(request)

    return redirect("login")


def create_Accountsignup(request):
    if request.method == "POST":
        # create new user for http POST
        form = accountSignupForm(request.POST)
        if form.is_valid:
            if form.data["password"] == form.data["password_confirmation"]:
                user = User.objects.create_user(
                    form.data["username"],
                    None,
                    form.data["password"],
                )
                if user is not None:
                    login(request, user)
                    return redirect("list_projects")
                else:
                    return redirect("signup")
            else:
                return HttpResponse("Password do not match")
    # if a GET (or any other method) we'll create a blank form
    else:
        form = accountSignupForm()
        context = {"form": form}

    return render(request, "registration/signup.html", context)
