from django.db import models


class accountList(models.Model):
    username = models.CharField(max_length=150)
    password = models.CharField(max_length=150)

    def __str__(self):
        return self.username


class accountSignupModel(models.Model):
    username = models.CharField(max_length=150)
    password = models.CharField(max_length=150)
    password_confirmation = models.CharField(max_length=150)

    def __str__(self):
        return self.username
