from django.urls import path
from accounts.views import (
    create_AccountLogin,
    create_AccountLogout,
    create_Accountsignup,
)

urlpatterns = [
    # path("", accountList, name="accountlist_list"),
    path("login/", create_AccountLogin, name="login"),
    path("logout/", create_AccountLogout, name="logout"),
    path("signup/", create_Accountsignup, name="signup"),
]
