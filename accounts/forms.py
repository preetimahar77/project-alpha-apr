from django.forms import ModelForm, PasswordInput, CharField
from accounts.models import accountList, accountSignupModel


class accountLoginForm(ModelForm):
    class Meta:
        model = accountList
        fields = "__all__"


class accountSignupForm(ModelForm):
    username = CharField(max_length=150)
    password = CharField(widget=PasswordInput, max_length=150)
    password_confirmation = CharField(widget=PasswordInput, max_length=150)

    class Meta:
        model = accountSignupModel
        fields = "__all__"
