from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth import get_user_model

# from django.forms import ModelForm

# Create your views here.


def list_projects(request):
    if request.user.is_authenticated:
        Project_list = Project.objects.filter(owner=request.user)
        context = {"Project_list": Project_list}
        return render(request, "projects/list.html", context)
    else:
        return redirect("login")


def show_project(request, id):
    if request.user.is_authenticated:
        model_instance = Project.objects.get(id=id)
        context = {"model_instance": model_instance}
        return render(request, "projects/detail.html", context)
    else:
        return redirect("login")


def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)

        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            form.save()
            return redirect("list_projects")

    else:
        form = ProjectForm()
        User = get_user_model()
        user_List = User.objects.all()
        context = {"form": form, "user_lists": user_List}

    return render(request, "projects/create.html", context)
