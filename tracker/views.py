from django.shortcuts import redirect


def redirect_project_list_view(request):
    response = redirect("/projects/")
    return response
