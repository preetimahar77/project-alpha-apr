from django.db import models

from projects.models import Project
from django.contrib.auth.models import User


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(auto_now_add=False)
    due_date = models.DateTimeField(auto_now_add=False)
    is_completed = models.BooleanField(default="False")
    project = models.ForeignKey(
        Project,
        related_name=("tasks"),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    assignee = models.ForeignKey(
        User,
        related_name=("tasks"),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name
