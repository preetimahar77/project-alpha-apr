from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from projects.models import Project
from tasks.models import Task
from django.http import HttpResponse

# from tasks.models import Task
from django.contrib.auth import get_user_model


# Create your views here.


def create_task(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = TaskForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect("list_projects")
            else:
                print(form.errors)
                return HttpResponse("error")

        else:
            form = TaskForm()
            User = get_user_model()
            Task_List = User.objects.all()
            projects_list = Project.objects.all()
            context = {
                "form": form,
                "tasks_assignee_lists": Task_List,
                "task_projects_lists": projects_list,
            }

            return render(request, "tasks/create.html", context)
    else:
        return redirect("login")


def show_my_tasks(request):
    if request.user.is_authenticated:
        Task_list = Task.objects.filter(assignee=request.user)
        context = {"task_list": Task_list}
        return render(request, "tasks/mine.html", context)
    else:
        return redirect("login")


def get_all_children(request):
    return Task.objects.filter(request=request.user)
